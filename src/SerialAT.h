/*
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef SERIAL_AT_H_INCLUDED
#define SERIAL_AT_H_INCLUDED

#include <Arduino.h>

/**
 * @brief module number,Now just AT module
 *
 */
typedef enum {
    ESP_AT_MODULE_NUM = 0x01
} esp_at_module;

/**
 * @brief subcategory number
 *
 */
typedef enum {
    ESP_AT_SUB_OK                       = 0x00,              /*!< OK */
    ESP_AT_SUB_COMMON_ERROR             = 0x01,
    ESP_AT_SUB_NO_TERMINATOR            = 0x02,              /*!<  not end with "\r\n" */
    ESP_AT_SUB_NO_AT                    = 0x03,              /*!<  not found AT or at or At or aT */
    ESP_AT_SUB_PARA_LENGTH_MISMATCH     = 0x04,              /*!<  parameter length not match */
    ESP_AT_SUB_PARA_TYPE_MISMATCH       = 0x05,              /*!<  parameter length not match */
    ESP_AT_SUB_PARA_NUM_MISMATCH        = 0x06,              /*!<  parameter number not match */
    ESP_AT_SUB_PARA_INVALID             = 0x07,
    ESP_AT_SUB_PARA_PARSE_FAIL          = 0x08,              /*!<  parse parameter fail */
    ESP_AT_SUB_UNSUPPORT_CMD            = 0x09,
    ESP_AT_SUB_CMD_EXEC_FAIL            = 0x0A,
    ESP_AT_SUB_CMD_PROCESSING           = 0x0B,              /*!<  previous command is processing */
    ESP_AT_SUB_CMD_OP_ERROR             = 0x0C,
} esp_at_error_code;

#define ESP_AT_ERROR_NO(subcategory,extension)  \
        ((ESP_AT_MODULE_NUM << 24) | ((subcategory) << 16) | (extension))

#define ESP_AT_CMD_ERROR_OK                                   ESP_AT_ERROR_NO(ESP_AT_SUB_OK,0x00)
#define ESP_AT_CMD_ERROR_NON_FINISH                           ESP_AT_ERROR_NO(ESP_AT_SUB_NO_TERMINATOR,0x00)
#define ESP_AT_CMD_ERROR_NOT_FOUND_AT                         ESP_AT_ERROR_NO(ESP_AT_SUB_NO_AT,0x00)
#define ESP_AT_CMD_ERROR_PARA_LENGTH(which_para)              ESP_AT_ERROR_NO(ESP_AT_SUB_PARA_LENGTH_MISMATCH,which_para)
#define ESP_AT_CMD_ERROR_PARA_TYPE(which_para)                ESP_AT_ERROR_NO(ESP_AT_SUB_PARA_TYPE_MISMATCH,which_para)
#define ESP_AT_CMD_ERROR_PARA_NUM(need,given)                 ESP_AT_ERROR_NO(ESP_AT_SUB_PARA_NUM_MISMATCH,(((need) << 8) | (given)))
#define ESP_AT_CMD_ERROR_PARA_INVALID(which_para)             ESP_AT_ERROR_NO(ESP_AT_SUB_PARA_INVALID,which_para)
#define ESP_AT_CMD_ERROR_PARA_PARSE_FAIL(which_para)          ESP_AT_ERROR_NO(ESP_AT_SUB_PARA_PARSE_FAIL,which_para)
#define ESP_AT_CMD_ERROR_CMD_UNSUPPORT                        ESP_AT_ERROR_NO(ESP_AT_SUB_UNSUPPORT_CMD,0x00)
#define ESP_AT_CMD_ERROR_CMD_EXEC_FAIL(result)                ESP_AT_ERROR_NO(ESP_AT_SUB_CMD_EXEC_FAIL,result)
#define ESP_AT_CMD_ERROR_CMD_PROCESSING                       ESP_AT_ERROR_NO(ESP_AT_SUB_CMD_PROCESSING,0x00)
#define ESP_AT_CMD_ERROR_CMD_OP_ERROR                         ESP_AT_ERROR_NO(ESP_AT_SUB_CMD_OP_ERROR,0x00)

class SerialATClass {
    public:
        SerialATClass(int, int);
        SerialATClass();

        void init();
        void parse_at_response(char *resp);
        int send_at(const char *fmt, ...);
        void handle_at();

    private:
        void parse_cwjap_resp(char *parm);

        int _rst_pin;
        int _mode_pin;
        char at_buf[4096];
        char* at_ptrs[16];
        int rcvd_lines;
        bool _initialized;

    protected:
        friend class ESP8266WiFiSTAClass;
        friend class ESP8266WiFiGenericClass;
};

#endif
