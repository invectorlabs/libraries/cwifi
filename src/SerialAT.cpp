/*
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#include <SerialAT.h>
#include <cWiFi.h>
#include <include/debug.h>

#define ESP8285_BAUD_RATE       115200
#define ESP                     Serial2

int wifi_rst = 33;
int wifi_mode = 39;

// -----------------------------------------------------------------------------
// Initialize class states and variables
SerialATClass::SerialATClass(int rst, int mode)
{
    _rst_pin = rst;
    _mode_pin = mode;

    // Make sure the wifi module is in reset
    digitalWrite(_rst_pin, LOW);
    // The mode pin must be high else the ESP8285 will enter flash download mode
    digitalWrite(_mode_pin, HIGH);
    pinMode(_rst_pin, OUTPUT);
    pinMode(_mode_pin, OUTPUT);
    _initialized = false;
}

SerialATClass::SerialATClass()
{
    _rst_pin = 33;
    _mode_pin = 39;

    // Make sure the wifi module is in reset
    digitalWrite(_rst_pin, LOW);
    // The mode pin must be high else the ESP8285 will enter flash download mode
    digitalWrite(_mode_pin, HIGH);
    pinMode(_rst_pin, OUTPUT);
    pinMode(_mode_pin, OUTPUT);
    _initialized = false;
}

// -----------------------------------------------------------------------------
// Initialize the serial port.
// For now we jut assume that this code will run on a Challenger board
// and that the ESP8285 has the proper firmware installed.
// So no errors are ever thrown
void SerialATClass::init()
{
    // On the Challenger board we always use serial port 2 at 115200.
    ESP.begin(ESP8285_BAUD_RATE);
    ESP.setTimeout(5000);

    DBG_PRINT("Initializing AT Uart !");
    _initialized = true;

    // Release reset and wait for the ready prompt
    digitalWrite(_rst_pin, LOW);
    delay(2);
    digitalWrite(_rst_pin, HIGH);
    String rdy = ESP.readStringUntil('\n');
    while(!rdy.startsWith("ready")) {
        yield();
        Serial.println(rdy);
        rdy = ESP.readStringUntil('\n');
    }
    // We do not want any echoes
    send_at("E0");
    // But we do want error codes
    send_at("+SYSLOG=1");
}

//
// Parse response from cwjap query.
// https://github.com/espressif/esp-at/blob/master/docs/en/get-started/ESP_AT_Commands_Set.md#cmd-JAP
//
//Query Command:
//
//AT+CWJAP?
//Function: to query the AP to which the ESP32 Station is already connected.
//
//Response:
//
//+CWJAP:<ssid>,<bssid>,<channel>,<rssi>,<pci_en>,<reconn>,<lisnten_interval>,<scan_mode>
//
void SerialATClass::parse_cwjap_resp(char *parm)
{
    const char s[2] = ",";
    char *token;

    // Go through all parameters in the response
    token = strtok(parm, s);
    // Copy SSID
    strncpy(cWiFi._ssid, token+1, strlen(token)-2);
    DBG_PRINT("SSID: %s", cWiFi._ssid);

    uint32_t mac[6];
    token = strtok(NULL, s)+1;
    DBG_PRINT("'%s'", token);
    sscanf(token, "%x:%x:%x:%x:%x:%x\"",
            &mac[0], &mac[1], &mac[2], &mac[3], &mac[4], &mac[5]);
    for (int i=0;i<6;i++)
        cWiFi._mac[i] = (uint8_t)mac[i];
    DBG_PRINT("MAC: %02x:%02x:%02x:%02x:%02x:%02x",
            mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

    token = strtok(NULL, s);
    cWiFi._channel = strtol(token, NULL, 10);
    DBG_PRINT("Channel: %d", cWiFi._channel);

    token = strtok(NULL, s);
    cWiFi._rssi = strtol(token, NULL, 10);
    DBG_PRINT("Rssi: %d", cWiFi._rssi);

    token = strtok(NULL, s);
    cWiFi._pci_en = strtol(token, NULL, 10);
    DBG_PRINT("PCI auth enabled: %d", cWiFi._pci_en);

    token = strtok(NULL, s);
    cWiFi._reconn = strtol(token, NULL, 10);
    DBG_PRINT("Reconnect: %d", cWiFi._reconn);

    token = strtok(NULL, s);
    cWiFi._listen_interval = strtol(token, NULL, 10);
    DBG_PRINT("Listen interval: %d", cWiFi._listen_interval);

    token = strtok(NULL, s);
    cWiFi._scan_mode = strtol(token, NULL, 10);
    DBG_PRINT("Scan Mode: %d", cWiFi._scan_mode);
}

//
// Parse an at response from the wifi modem
// This will take care of the response and update necessary variables
void SerialATClass::parse_at_response(char *resp)
{
    if (!strncmp(resp, "WIFI CONNECTED", 14)) {
        cWiFi._status = WL_CONNECTED;
    } if (!strncmp(resp, "WIFI GOT IP", 11)) {
        cWiFi._ip_valid = true;
    } if (!strncmp(resp, "WIFI DISCONNECT", 15)) {
        cWiFi._status = WL_DISCONNECTED;
        cWiFi._ip_valid = false;
    } if (!strncmp(resp, "+CWMODE:", 8)) {
        cWiFi._mode = (WiFiMode_t)strtol(&resp[8], NULL, 10);
    } if (!strncmp(resp, "+CWJAP:", 7)) {
        parse_cwjap_resp(&resp[7]);
    } if (!strncmp(resp, "+CIPDOMAIN:", 11)) {
        uint32_t ip[4];
        sscanf(&resp[11], "%u.%u.%u.%u", &ip[0], &ip[1], &ip[2], &ip[3]);
        cWiFi._hostIP = IPAddress(ip[0], ip[1], ip[2], ip[3]);
    } if (!strncmp(resp, "+SLEEP:", 7)) {
        cWiFi._sleep_type = (WiFiSleepType_t)strtol(&resp[7], NULL, 10);
    }
}

//
// Sends an AT command and stores the resulting list in the at_buf, with
// at_ptrs that points to each incoming line. Is this good ? No idea.
// The function will call at parser to parse any response from the 8285.
int SerialATClass::send_at(const char *fmt, ...)
{
    int ret = false;
    char at_out[64];
    va_list myargs;
    va_start(myargs, fmt);

    if (!_initialized)
        init();

    /* Forward the '...' to vsprintf */
    vsprintf(at_out, fmt, myargs);
    ESP.printf("AT%s\r\n", at_out);
    INF_PRINT("S:AT%s", at_out);

    // Wait for "OK" or "ERROR" from the wifi chip
    ESP.readBytesUntil('\n', at_buf, 64);           // Read response
    char *trim = strchr(at_buf, '\r');              // Find first '\r'
    *trim = '\0';                                   // Terminate string there
    int rcvd = strlen(at_buf);                      // Count characters received
    int offset = 0;
    rcvd_lines = 0;
    at_ptrs[rcvd_lines] = &at_buf[offset];          // Store this line
    while (strncmp(&at_buf[offset], "OK", 2) && strncmp(&at_buf[offset], "ERROR", 5)) {
        INF_PRINT("R:%s", &at_buf[offset]);
        parse_at_response(&at_buf[offset]);
        offset += rcvd + 1; // +1 because of the line terminator (0)
        ESP.readBytesUntil('\n', &at_buf[offset], 64);
        trim = strchr(&at_buf[offset], '\r');       // Find first '\r'
        *trim = '\0';                               // terminate string
        rcvd = strlen(&at_buf[offset]);             // How many bytes were rcvd
        rcvd_lines++;
        at_ptrs[rcvd_lines] = &at_buf[offset];
    }
    if (!strncmp(&at_buf[offset], "OK", 2)) ret = true;
    /* Clean up the va_list */
    INF_PRINT("C:%s\n", &at_buf[offset]);
    va_end(myargs);

    return ret;
}

//
// This function needs to be called repetadly from the main loop.
// It is responsible for handling incoming unsolicited messages from
// the ESP8285.
void SerialATClass::handle_at()
{
    char *trim;
    if (!_initialized)
        init();
    // If there's data on the port we need to get it and process it.
    // Here we only process one line at a time.
    if (ESP.available()) {
        ESP.readBytesUntil('\n', at_buf, 64);
        trim = strchr(at_buf, '\r');       // Find first '\r'
        *trim = '\0';                               // terminate string
        DBG_PRINT("Unsolicited data: %s\n", at_buf);
        parse_at_response(at_buf);
    }
}
