/*
 ESP8266WiFiSTA.cpp - WiFi library for esp8266

 Copyright (c) 2014 Ivan Grokhotkov. All rights reserved.
 This file is part of the esp8266 core for Arduino environment.

 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

 Reworked on 28 Dec 2015 by Markus Sattler

 */

#include "cWiFi.h"
#include "ESP8266WiFiGeneric.h"
#include "ESP8266WiFiSTA.h"
#include "include/user_interface.h"

// -----------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------- Private functions ------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------------------
// ---------------------------------------------------- STA function -----------------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------

bool ESP8266WiFiSTAClass::_useStaticIp = false;
bool ESP8266WiFiSTAClass::_useInsecureWEP = false;



/**
 * Start Wifi connection
 * if passphrase is set the most secure supported mode will be automatically selected
 * @param ssid const char*          Pointer to the SSID string.
 * @param passphrase const char *   Optional. Passphrase. Valid characters in a passphrase must be between ASCII 32-126 (decimal).
 * @param bssid uint8_t[6]          Optional. BSSID / MAC of AP
 * @param channel                   Optional. Channel of AP
 * @param connect                   Optional. call connect
 * @return
 */

wl_status_t ESP8266WiFiSTAClass::begin(const char* ssid, const char *passphrase, int32_t channel, const uint8_t* bssid, bool connect)
{
    bool resp;
    wl_status_t ret = WL_CONNECTED;

    resp = cWiFi.send_at("+CWJAP=\"%s\",\"%s\"", ssid, passphrase);
    if (!resp) {
        ret = WL_CONNECT_FAILED;
    }
    // Make sure internal structures are updated
    cWiFi.send_at("+CWJAP?");

    return ret;
}

wl_status_t ESP8266WiFiSTAClass::begin(char* ssid, char *passphrase, int32_t channel, const uint8_t* bssid, bool connect) {
    return begin((const char*) ssid, (const char*) passphrase, channel, bssid, connect);
}

wl_status_t ESP8266WiFiSTAClass::begin(const String& ssid, const String& passphrase, int32_t channel, const uint8_t* bssid, bool connect) {
    return begin(ssid.c_str(), passphrase.c_str(), channel, bssid, connect);
}

/*
 * AT+CWJAP—Connects to an AP
 *
 * Query Command:
 *
 * AT+CWJAP?
 * Function: to query the AP to which the ESP32 Station is already connected.
 *
 * Response:
 *
 * +CWJAP:<ssid>,<bssid>,<channel>,<rssi>,<pci_en>,<reconn>,<lisnten_interval>,<scan_mode>
 * OK
 *
 * Parameters:
 *
 * <ssid>: a string parameter showing the SSID of the AP.
 *     <bssid>: the AP’s MAC address.
 *     <channel>: channel
 *     <rssi>: signal strength
 *     [<pci_en>]: PCI Authentication, which will disable connect OPEN and WEP AP.
 *     [<reconn>]: Wi-Fi reconnection, when beacon timeout, ESP32 will reconnect automatically.
 *     [<listen_interval>]: the interval of listening to the AP's beacon,the range is (0,100]
 *     [<scan_mode>]:
 *         0: Do fast scan, scan will end after find SSID match AP, Wi-Fi will connect the first scanned AP.
 *         1: All channel scan, scan will end after scan all the channel, Wi-Fi will connect the AP with the strongest signal scanned.
 *
 * Set Command:
 *
 * AT+CWJAP=<ssid>,<pwd>[,<bssid>][,<pci_en>][,<reconn>][,<listen_interval>][,<scan_mode>]
 * Function: to set the AP to which the ESP32 Station needs to be connected.
 *
 * Response:
 *
 * OK
 *
 * or +CWJAP: ERROR Parameters:
 *
 *     <ssid>: the SSID of the target AP.
 *         Escape character syntax is needed if SSID or password contains any special characters, such as , or " or \.
 * <pwd>: password, MAX: 64-byte ASCII.
 *     [<bssid>]: the target AP's MAC address, used when multiple APs have the same SSID.
 *     [<pci_en>]: enable PCI Authentication, which will disable connect OPEN and WEP AP.
 *     [<reconn>]: enable Wi-Fi reconnection, when beacon timeout, ESP32 will reconnect automatically.
 *     [<listen_interval>]: the interval of listening to the AP's beacon,the range is (0,100], by default, the value is 3.
 *     <error code>: (for reference only)
 *         1: connection timeout.
 *         2: wrong password.
 *         3: cannot find the target AP.
 *         others: unknown error occurred.
 *         4: connection failed.
 *     [<scan_mode>]:
 *         0: Do fast scan, scan will end after find SSID match AP, Wi-Fi will connect the first scanned AP.
 *         1: All channel scan, scan will end after scan all the channel, Wi-Fi will connect the AP with the strongest signal scanned.
 *
 * Note:
 *
 *     The configuration changes will be saved in the NVS area if AT+SYSSTORE=1.
 *     This command requires Station mode to be active.
*/
wl_status_t ESP8266WiFiSTAClass::begin(bool reconn, const char* ssid, const char *passphrase, const char* bssid, bool pci_en, int listen_interval, int scan_mode) {
    bool resp;
    wl_status_t ret = WL_CONNECTED;

    if (bssid) {
        // AT+CWJAP=<ssid>,<pwd>[,<bssid>][,<pci_en>][,<reconn>][,<listen_interval>][,<scan_mode>]
        resp = cWiFi.send_at("+CWJAP=\"%s\",\"%s\",\"%s\",%d,%d,%d,%d", ssid, passphrase, bssid, pci_en, reconn, listen_interval, scan_mode);
    } else {
        resp = cWiFi.send_at("+CWJAP=\"%s\",\"%s\",,%d,%d,%d,%d", ssid, passphrase, pci_en, reconn, listen_interval, scan_mode);
    }
    if (!resp) {
        ret = WL_CONNECT_FAILED;
    }
    // Make sure internal structures are updated
    cWiFi.send_at("+CWJAP?");

    return ret;
}

wl_status_t ESP8266WiFiSTAClass::begin(bool reconn, char* ssid, char *passphrase, const char* bssid, bool pci_en, int listen_interval, int scan_mode) {
    return begin(reconn, (const char *)ssid, (const char *)passphrase, bssid, pci_en, listen_interval, scan_mode);
}

wl_status_t ESP8266WiFiSTAClass::begin(bool reconn, const String& ssid, const String& passphrase, const char* bssid, bool pci_en, int listen_interval, int scan_mode) {
    return begin(reconn, ssid.c_str(), passphrase.c_str(), bssid, pci_en, listen_interval, scan_mode);
}

/**
 * Use to connect to SDK config.
 * @return wl_status_t
 */
wl_status_t ESP8266WiFiSTAClass::begin() {
    return status();
}

//
// AT+CIPSTA—Sets the IP Address of the ESP32 Station
//
// Query Command:
//
// AT+CIPSTA?
// Function: to obtain the IP address of the ESP32 Station.
// Notice: Only when the ESP32 Station is connected to an AP can its IP address be queried.
//
// Response:
//
// +CIPSTA:<ip>
// OK
//
// Set Command:
//
// AT+CIPSTA=<ip>[,<gateway>,<netmask>]
// Function: to set the IP address of the ESP32 Station.
//
// Response:
//
// OK
//
// Parameters:
//
//    <ip>: string parameter, the IP address of the ESP32 Station.
//    [<gateway>]: gateway.
//    [<netmask>]: netmask.
//
// Notes:
//
//    The configuration changes will be saved in the NVS area if AT+SYSSTORE=1.
//    The set command interacts with DHCP-related AT commands (AT+CWDHCP-related commands):
//        If static IP is enabled, DHCP will be disabled;
//        If DHCP is enabled, static IP will be disabled;
//        Whether it is DHCP or static IP that is enabled depends on the last configuration.
//
bool ESP8266WiFiSTAClass::config(IPAddress local_ip, IPAddress arg1, IPAddress arg2, IPAddress arg3, IPAddress dns2) {

    bool resp;

    resp = cWiFi.send_at("+CIPSTA=\"%d.%d.%d.%d\",\"%d.%d.%d.%d\",\"%d.%d.%d.%d\"",
                local_ip[0], local_ip[1], local_ip[2], local_ip[3],
                arg1[0], arg1[1], arg1[2], arg1[3],
                arg2[0], arg2[1], arg2[2], arg2[3]);

    // Do DNS's
    if (arg3 && dns2) {
        cWiFi.send_at("+CIPDNS=1,\"%d.%d.%d.%d\",\"%d.%d.%d.%d\"",
                arg3[0], arg3[1], arg3[2], arg3[3], dns2[0], dns2[1], dns2[2], dns2[3]);
    } else if (arg3) {
        cWiFi.send_at("+CIPDNS=1,\"%d.%d.%d.%d\"",
                arg3[0], arg3[1], arg3[2], arg3[3]);
    }

    return resp;
}

bool ESP8266WiFiSTAClass::getConfig(IPAddress& local_ip, IPAddress& gateway, IPAddress& subnet) {
    bool resp;

    resp = cWiFi.send_at("+CIPSTA?");
    // The results are returned one for each line.
    for (int i=0;i<3;i++) {
        if (!strncmp(cWiFi.at_ptrs[i], "+CIPSTA:ip:", 11)) {
            uint32_t ip[4];
            char *ptr = strchr(cWiFi.at_ptrs[i], '\"') + 1;
            sscanf(ptr, "%u.%u.%u.%u", &ip[0], &ip[1], &ip[2], &ip[3]);
            local_ip = IPAddress(ip[0], ip[1], ip[2], ip[3]);
        } else if (!strncmp(cWiFi.at_ptrs[i], "+CIPSTA:gateway:", 16)) {
            uint32_t ip[4];
            char *ptr = strchr(cWiFi.at_ptrs[i], '\"') + 1;
            sscanf(ptr, "%u.%u.%u.%u", &ip[0], &ip[1], &ip[2], &ip[3]);
            gateway = IPAddress(ip[0], ip[1], ip[2], ip[3]);
        } else if (!strncmp(cWiFi.at_ptrs[i], "+CIPSTA:netmask:", 16)) {
            uint32_t ip[4];
            char *ptr = strchr(cWiFi.at_ptrs[i], '\"') + 1;
            sscanf(ptr, "%u.%u.%u.%u", &ip[0], &ip[1], &ip[2], &ip[3]);
            subnet = IPAddress(ip[0], ip[1], ip[2], ip[3]);
        }
    }
    return resp;
}

/**
 * will force a disconnect an then start reconnecting to AP
 * This will work slightly different from the standard ESP8266 environment.
 * Reconnect is specified on the connect command (AT+CWJAP) so for this
 * function to work reconnect must be specified when connecting.
 * This function will simply disconnect from the network.
 * @return ok
 */
bool ESP8266WiFiSTAClass::reconnect() {
    // First we force a disconnect
    return disconnect(true);
}

/**
 * Disconnect from the network
 * @param wifioff
 * @return  Success or fail of operation
 */
bool ESP8266WiFiSTAClass::disconnect(bool wifioff) {
    bool resp = false;

    if (wifioff) {
        resp = cWiFi.send_at("+CWQAP");
        if (resp)
            cWiFi._status == WL_DISCONNECTED;
    }
    return resp;
}

/**
 * is STA interface connected?
 * @return true if STA is connected to an AD
 */
bool ESP8266WiFiSTAClass::isConnected() {
    return (cWiFi._status == WL_CONNECTED);
}


//
//  Setting the ESP8266 station to connect to the AP (which is recorded)
//  automatically or not when powered on. Enable auto-connect by default.
//  @param autoConnect bool
//  AT+CWAUTOCONN—Auto-Connects to the AP or Not
//
//  Set Command:
//
//  AT+CWAUTOCONN=<enable>
//
//  Response:
//
//  OK
//
//  Parameters:
//
//      <enable>:
//          0: does NOT auto-connect to AP on power-up.
//          1: connects to AP automatically on power-up.
//
//  Note:
//
//      The configuration changes will be saved in the NVS area.
//      The ESP32 Station connects to the AP automatically on power-up by default.
//  @return if saved
//
bool ESP8266WiFiSTAClass::setAutoConnect(bool autoConnect) {
    bool resp;
    resp = cWiFi.send_at("+CWAUTOCONN=%d", (int)autoConnect);
    return resp;
}

/**
 * Checks if ESP8266 station mode will connect to AP
 * automatically or not when it is powered on.
 * @return auto connect
 */
bool ESP8266WiFiSTAClass::getAutoConnect() {
    bool resp;
    int ret;
    resp = cWiFi.send_at("+CWAUTOCONN?");

    if (resp && !strncmp(cWiFi.at_ptrs[0], "+CWAUTOCONN", 11))
        ret = strtol(strchr(cWiFi.at_ptrs[0], ':')+1, NULL, 10);

    return (bool)ret;
}

/**
 * Set whether reconnect or not when the ESP8266 station is disconnected from AP.
 * This is all handled when connecting to the AP. This function is only here
 * for compatability but will false when called.
 * @param autoReconnect
 * @return
 */
bool ESP8266WiFiSTAClass::setAutoReconnect(bool autoReconnect) {
    return false;
}

/**
 * get whether reconnect or not when the ESP8266 station is disconnected from AP.
 * @return autoreconnect
 */
bool ESP8266WiFiSTAClass::getAutoReconnect() {

    return cWiFi._reconn;
}

/**
 * Wait for WiFi connection to reach a result
 * returns the status reached or disconnect if STA is off
 * This function has no meaning when interfacing to an AT modem, all will
 * be handled by the modem itself. It will return the current status
 * @return wl_status_t or -1 on timeout
 */
wl_status_t ESP8266WiFiSTAClass::waitForConnectResult(unsigned long timeoutLength) {
    return cWiFi._status;
}

/**
 * Get the station interface IP address.
 * @return IPAddress station IP
 */
IPAddress ESP8266WiFiSTAClass::localIP() {
    IPAddress localIP;
    IPAddress gateway;
    IPAddress netmask;
    getConfig(localIP, gateway, netmask);

    return IPAddress(localIP[0], localIP[1], localIP[2], localIP[3]);
}

/**
 * Get the station interface MAC address.
 * @param mac   pointer to uint8_t array with length WL_MAC_ADDR_LENGTH
 * @return      pointer to uint8_t *
 */
uint8_t* ESP8266WiFiSTAClass::macAddress(uint8_t* mac) {
#if 0
    wifi_get_macaddr(STATION_IF, mac);
    return mac;
#endif
    return mac;
}

/**
 * Get the station interface MAC address.
 * @return String mac
 */
String ESP8266WiFiSTAClass::macAddress(void) {
#if 0
    uint8_t mac[6];
    char macStr[18] = { 0 };
    wifi_get_macaddr(STATION_IF, mac);

    sprintf(macStr, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
    return String(macStr);
#endif
    return String("Hello");
}

/**
 * Get the interface subnet mask address.
 * @return IPAddress subnetMask
 */
IPAddress ESP8266WiFiSTAClass::subnetMask() {
#if 0
    struct ip_info ip;
    wifi_get_ip_info(STATION_IF, &ip);
    return IPAddress(ip.netmask.addr);
#endif
    return IPAddress();
}

/**
 * Get the gateway ip address.
 * @return IPAddress gatewayIP
 */
IPAddress ESP8266WiFiSTAClass::gatewayIP() {
#if 0
    struct ip_info ip;
    wifi_get_ip_info(STATION_IF, &ip);
    return IPAddress(ip.gw.addr);
#endif
    return IPAddress();
}

/**
 * Get the DNS ip address.
 * @param dns_no
 * @return IPAddress DNS Server IP
 */
IPAddress ESP8266WiFiSTAClass::dnsIP(uint8_t dns_no) {
#if 0
#if LWIP_VERSION_MAJOR == 1
    ip_addr_t dns_ip = dns_getserver(dns_no);
    return IPAddress(dns_ip.addr);
#else
    return IPAddress(dns_getserver(dns_no));
#endif
#endif
    return IPAddress();
}


/**
 * Get ESP8266 station DHCP hostname
 * @return hostname
 */
String ESP8266WiFiSTAClass::hostname(void) {
#if 0
    return wifi_station_get_hostname();
#endif
    return String("");
}

/**
 * Set ESP8266 station DHCP hostname
 * @param aHostname max length:24
 * @return ok
 */
bool ESP8266WiFiSTAClass::hostname(const char* aHostname) {
  /*
  vvvv RFC952 vvvv
  ASSUMPTIONS
  1. A "name" (Net, Host, Gateway, or Domain name) is a text string up
   to 24 characters drawn from the alphabet (A-Z), digits (0-9), minus
   sign (-), and period (.).  Note that periods are only allowed when
   they serve to delimit components of "domain style names". (See
   RFC-921, "Domain Name System Implementation Schedule", for
   background).  No blank or space characters are permitted as part of a
   name. No distinction is made between upper and lower case.  The first
   character must be an alpha character.  The last character must not be
   a minus sign or period.  A host which serves as a GATEWAY should have
   "-GATEWAY" or "-GW" as part of its name.  Hosts which do not serve as
   Internet gateways should not use "-GATEWAY" and "-GW" as part of
   their names. A host which is a TAC should have "-TAC" as the last
   part of its host name, if it is a DoD host.  Single character names
   or nicknames are not allowed.
  ^^^^ RFC952 ^^^^

  - 24 chars max
  - only a..z A..Z 0..9 '-'
  - no '-' as last char
  */

#if 0
    size_t len = strlen(aHostname);

    if (len == 0 || len > 32) {
        // nonos-sdk limit is 32
        // (dhcp hostname option minimum size is ~60)
        DEBUG_WIFI_GENERIC("cWiFi.(set)hostname(): empty or large(>32) name\n");
        return false;
    }

    // check RFC compliance
    bool compliant = (len <= 24);
    for (size_t i = 0; compliant && i < len; i++)
        if (!isalnum(aHostname[i]) && aHostname[i] != '-')
            compliant = false;
    if (aHostname[len - 1] == '-')
        compliant = false;

    if (!compliant) {
        DEBUG_WIFI_GENERIC("hostname '%s' is not compliant with RFC952\n", aHostname);
    }

    bool ret = wifi_station_set_hostname(aHostname);
    if (!ret) {
        DEBUG_WIFI_GENERIC("cWiFi.hostname(%s): wifi_station_set_hostname() failed\n", aHostname);
        return false;
    }

    // now we should inform dhcp server for this change, using lwip_renew()
    // looping through all existing interface
    // harmless for AP, also compatible with ethernet adapters (to come)
    for (netif* intf = netif_list; intf; intf = intf->next) {

        // unconditionally update all known interfaces
#if LWIP_VERSION_MAJOR == 1
        intf->hostname = (char*)wifi_station_get_hostname();
#else
        intf->hostname = wifi_station_get_hostname();
#endif

        if (netif_dhcp_data(intf) != nullptr) {
            // renew already started DHCP leases
            err_t lwipret = dhcp_renew(intf);
            if (lwipret != ERR_OK) {
                DEBUG_WIFI_GENERIC("cWiFi.hostname(%s): lwIP error %d on interface %c%c (index %d)\n",
                                   intf->hostname, (int)lwipret, intf->name[0], intf->name[1], intf->num);
                ret = false;
            }
        }
    }

    return ret && compliant;
#endif
    return false;
}

/**
 * Return Connection status.
 * @return one of the value defined in wl_status_t
 *
 */
wl_status_t ESP8266WiFiSTAClass::status() {
#if 0
    station_status_t status = wifi_station_get_connect_status();

    switch(status) {
        case STATION_GOT_IP:
            return WL_CONNECTED;
        case STATION_NO_AP_FOUND:
            return WL_NO_SSID_AVAIL;
        case STATION_CONNECT_FAIL:
        case STATION_WRONG_PASSWORD:
            return WL_CONNECT_FAILED;
        case STATION_IDLE:
            return WL_IDLE_STATUS;
        default:
            return WL_DISCONNECTED;
    }
#endif
    return WL_DISCONNECTED;
}

/**
 * Return the current SSID associated with the network
 * @return SSID
 */
String ESP8266WiFiSTAClass::SSID() const {
#if 0
    struct station_config conf;
    wifi_station_get_config(&conf);
    char tmp[33]; //ssid can be up to 32chars, => plus null term
    memcpy(tmp, conf.ssid, sizeof(conf.ssid));
    tmp[32] = 0; //nullterm in case of 32 char ssid
    return String(reinterpret_cast<char*>(tmp));
#endif
    return String("");
}

/**
 * Return the current pre shared key associated with the network
 * @return  psk string
 */
String ESP8266WiFiSTAClass::psk() const {
#if 0
    struct station_config conf;
    wifi_station_get_config(&conf);
    char tmp[65]; //psk is 64 bytes hex => plus null term
    memcpy(tmp, conf.password, sizeof(conf.password));
    tmp[64] = 0; //null term in case of 64 byte psk
    return String(reinterpret_cast<char*>(tmp));
#endif
    return String("");
}

/**
 * Return the current bssid / mac associated with the network if configured
 * @return bssid uint8_t *
 */
uint8_t* ESP8266WiFiSTAClass::BSSID(void) {
#if 0
    static struct station_config conf;
    wifi_station_get_config(&conf);
    return reinterpret_cast<uint8_t*>(conf.bssid);
#endif
    uint8_t t;
    return &t;
}

/**
 * Return the current bssid / mac associated with the network if configured
 * @return String bssid mac
 */
String ESP8266WiFiSTAClass::BSSIDstr(void) {
    char mac[18] = { 0 };
    cWiFi.send_at("+CWJAP?");
    sprintf(mac, "%02X:%02X:%02X:%02X:%02X:%02X", cWiFi._mac[0], cWiFi._mac[1], cWiFi._mac[2], cWiFi._mac[3], cWiFi._mac[4], cWiFi._mac[5]);
    return String(mac);
}

/**
 * Return the current network RSSI.
 * @return  RSSI value
 */
int32_t ESP8266WiFiSTAClass::RSSI(void) {
    cWiFi.send_at("+CWJAP?");
    return cWiFi._rssi;
}



// -----------------------------------------------------------------------------------------------------------------------
// -------------------------------------------------- STA remote configure -----------------------------------------------
// -----------------------------------------------------------------------------------------------------------------------

bool ESP8266WiFiSTAClass::_smartConfigStarted = false;
bool ESP8266WiFiSTAClass::_smartConfigDone = false;

/**
 * Start SmartConfig
 */
bool ESP8266WiFiSTAClass::beginSmartConfig() {
#if 0
    if(_smartConfigStarted) {
        return false;
    }

    if(!cWiFi.enableSTA(true)) {
        // enable STA failed
        return false;
    }

    if(smartconfig_start(reinterpret_cast<sc_callback_t>(&ESP8266WiFiSTAClass::_smartConfigCallback), 1)) {
        _smartConfigStarted = true;
        _smartConfigDone = false;
        return false;
    }
#endif
    return false;
}


/**
 *  Stop SmartConfig
 */
bool ESP8266WiFiSTAClass::stopSmartConfig() {
#if 0
    if(!_smartConfigStarted) {
        return true;
    }

    if(smartconfig_stop()) {
        _smartConfigStarted = false;
        return true;
    }
    #endif
    return false;
}

/**
 * Query SmartConfig status, to decide when stop config
 * @return smartConfig Done
 */
bool ESP8266WiFiSTAClass::smartConfigDone() {
#if 0
    if(!_smartConfigStarted) {
        return false;
    }

    return _smartConfigDone;
#endif
    return false;
}


/**
 * _smartConfigCallback
 * @param st
 * @param result
 */
void ESP8266WiFiSTAClass::_smartConfigCallback(uint32_t st, void* result) {
}
