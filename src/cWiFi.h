/*
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation; either
 version 2.1 of the License, or (at your option) any later version.

 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.

 You should have received a copy of the GNU Lesser General Public
 License along with this library; if not, write to the Free Software
 Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/

#ifndef CWIFI_H_INCLUDED
#define CWIFI_H_INCLUDED

#include <Arduino.h>

#include <include/wl_definitions.h>
#include <include/wl_types.h>

#include "IPAddress.h"

#include "ESP8266WiFiType.h"
#include "ESP8266WiFiSTA.h"
#include "ESP8266WiFiGeneric.h"
#include "SerialAT.h"

class cWiFi_class : public ESP8266WiFiGenericClass, public ESP8266WiFiSTAClass, public SerialATClass {
    public:
        cWiFi_class();

    private:
        int _channel;

    protected:
        friend class ESP8266WiFiGenericClass;
        friend class SerialATClass;
};

extern cWiFi_class cWiFi;

#endif
